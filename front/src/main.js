import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import { useAuthStore } from './stores'

const app = createApp(App)

app.use(createPinia())

const authStore = useAuthStore()
authStore.login('John', 'Doe')

app.mount('#app')
