import { defineStore } from 'pinia'
import CryptoJS from 'crypto-js'

const baseUrl = `${import.meta.env.VITE_API_URL}/auth`

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    user: JSON.parse(localStorage.getItem('user')),
    returnUrl: null
  }),
  actions: {
    async login(username, password) {
      // Pretend we are getting JWT from backend 🙃
      // const user = await fetchWrapper.post(`${baseUrl}/login`, {username, password});
      const token = generateJWT(header, payload, secret)
      const user = {
        token: token
      }
      this.user = user
      localStorage.setItem('user', JSON.stringify(user))
    }
  }
})

const header = {
  alg: 'HS256',
  typ: 'JWT'
}

const payload = {
  user: 'John Doe'
}

const secret = import.meta.env.JWT_SECRET || 'mcb3DaS05Yvt'

const generateJWT = (header, payload, secret) => {
  const base64Header = btoa(JSON.stringify(header)).replaceAll('=', '')
  const base64Payload = btoa(JSON.stringify(payload)).replaceAll('=', '')
  const hmac = CryptoJS.HmacSHA256(`${base64Header}.${base64Payload}`, secret).toString(
    CryptoJS.enc.Base64
  )
  const base64Sign = hmac.replaceAll('=', '')

  return `${base64Header}.${base64Payload}.${base64Sign}`
}
