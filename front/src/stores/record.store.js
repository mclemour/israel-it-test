import { defineStore } from 'pinia'
import { fetchWrapper } from '@/helpers'

const baseUrl = `${import.meta.env.VITE_API_URL}/api/v1/redis`

export const useRecordsStore = defineStore({
  id: 'records',
  state: () => ({
    records: [],
    total: 0
  }),
  actions: {
    async getRecords(offset = 0, limit = 10) {
      this.records = { loading: true }
      fetchWrapper
        .get(`${baseUrl}?offset=${offset}&limit=${limit}`)
        .then((records) => {
          this.records = records.data
          this.total = records.total
        })
        .catch((error) => (this.records = { error }))
    }
  }
})
