'use strict';

const DELAY_MS = 1000;
const CONCURRENCY = 5;

const childProcess = require('node:child_process');
const nodecrypto = require('node:crypto');

const url = 'http://localhost:8000/api/v1/redis';
const maxTime = 10;
const connectTimeout = 10;

const curl = (data: object) => `curl\
    --max-time ${maxTime} \
    --connect-timeout ${connectTimeout} \
    -iSs \
    -d ${encodeURI(JSON.stringify(data))} \
    -H "Content-Type: application/json" \
    -X POST \
    ${url}`;

const sendRequest = () => {
  for (let i = 0; i < CONCURRENCY; i++) {
    const randomString = nodecrypto.randomBytes(20).toString('hex');
    const data = {
      data: randomString,
    };

    childProcess.exec(curl(data), (err: any, stdout: any, stderr: any) => {
      if (err) console.log('Error: ', err);
      if (stderr) console.log('Stderr: ', stderr);
      console.log('Stdout: ', stdout);
    });
  }
};

setInterval(sendRequest, DELAY_MS);

module.exports = [];
