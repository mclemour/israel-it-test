'use strict';

import { execSync } from 'node:child_process';
import { IDefaultQueryParams, IKeyValueStorage } from 'lib/types';

export const DEFAULT_QUERY_PARAMS: IDefaultQueryParams = {
  offset: 0,
  limit: 10,
  order: 'desc',
};

const REDIS_HOST = process.env.REDIS_HOST || 'redis';
const REDIS_PORT = process.env.REDIS_PORT || '6379';

const redisCli = (command: string) =>
  `/usr/bin/redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} ${command}`;

export class RedisCliShell<K, V> implements IKeyValueStorage<K, V> {
  private _keyHash: string;

  constructor() {
    this._keyHash = 'lorem-ipsum';
  }

  async get(key: K): Promise<string> {
    const command = redisCli(`get "${this._keyHash}:${String(key)}"`);
    const result = execSync(command);
    return result.toString();
  }
  async set(key: K, value: V): Promise<string> {
    const command = redisCli(
      `set "${this._keyHash}:${String(key)}" "${value}"`
    );
    const result = execSync(command);
    return result.toString();
  }
  async getset(key: K, value: V): Promise<V | undefined> {
    const commandGet = redisCli(`get "${this._keyHash}:${String(key)}"`);
    const commandSet = redisCli(
      `set "${this._keyHash}-${String(key)}" "${value}"`
    );
    const result = execSync(commandGet);
    execSync(commandSet);
    return new Promise((res) => res(result.toString() as V));
  }
  async delete(key: K): Promise<string> {
    const command = redisCli(`del "${this._keyHash}:${String(key)}"`);
    const result = execSync(command);
    return result.toString();
  }
  async zadd(collection: string, key: K): Promise<string> {
    const score = String(Math.floor(Date.now() / 1000));
    const command = redisCli(
      `zadd "${this._keyHash}:${collection}" ${score} "${
        this._keyHash
      }:${String(key)}"`
    );
    const result = execSync(command);
    return result.toString();
  }
  async mget(keys: K[]): Promise<string[]> {
    const formattedKeys = keys.map((k) => `${this._keyHash}:${k}`).join(' ');
    const command = redisCli(`mget ${formattedKeys}`);
    const execRes = execSync(command);
    return execRes
      .toString()
      .split('\n')
      .filter((el) => el !== '');
  }
  async zrange(
    collection: string,
    offset: number = DEFAULT_QUERY_PARAMS.offset,
    limit: number = DEFAULT_QUERY_PARAMS.limit,
    order: 'asc' | 'desc' = DEFAULT_QUERY_PARAMS.order
  ): Promise<string[]> {
    const com = order === 'asc' ? 'zrange' : 'zrevrange';
    const command = redisCli(
      `${com} "${this._keyHash}:${collection}" ${String(offset)} ${String(
        offset + limit
      )}`
    );
    const execRes = execSync(command);
    const keys = execRes
      .toString()
      .split('\n')
      .filter((el) => el !== '')
      .map((key) => key.replace(`${this._keyHash}:`, ''));

    return keys;
  }
  async zcard(collection: string): Promise<number> {
    const command = redisCli(`zcard "${this._keyHash}:${collection}"`);
    const execRes = execSync(command);
    const total = parseInt(execRes.toString());
    return total;
  }
}
