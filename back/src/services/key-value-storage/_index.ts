'use strict';

import { KEY_VALUE_STORAGE } from 'lib/types';
import { KeyValueStorageMock } from './mock';
import { RedisCliShell } from './redis-cli-shell';

export const keyValueStorageFactory = <K, V>(vendor: KEY_VALUE_STORAGE) => {
  return {
    [KEY_VALUE_STORAGE.MOCK]: KeyValueStorageMock<K, V>,
    [KEY_VALUE_STORAGE.REDIS_CLI_SHELL]: RedisCliShell<K, V>,
  }[vendor];
};
