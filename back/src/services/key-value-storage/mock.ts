'use strict';

import { IKeyValueStorage } from 'lib/types';

export class KeyValueStorageMock<K, V> implements IKeyValueStorage<K, V> {
  private storage: Map<K, V>;

  constructor() {
    this.storage = new Map<K, V>();
  }

  async get(key: K): Promise<string> {
    return String(this.storage.get(key));
  }
  async set(key: K, value: V): Promise<string> {
    this.storage.set(key, value);
    return 'OK';
  }
  async getset(key: K, value: V): Promise<V | undefined> {
    const result = this.storage.get(key);
    this.storage.set(key, value);
    return result;
  }
  async delete(key: K): Promise<string> {
    this.storage.delete(key);
    return '1';
  }
  async zadd(collection: string, key: K): Promise<string> {
    return '(nil)';
  }
  async mget(keys: K[]): Promise<string[]> {
    return [];
  }
  async zrange(
    collection: string,
    offset: number,
    limit: number,
    order: 'asc' | 'desc'
  ): Promise<string[]> {
    return ['(nil)'];
  }
  async zcard(collection: string): Promise<number> {
    return Object.keys(this.storage).length;
  }
}
