'use strict';

import { IncomingMessage } from 'http';
import crypto from 'node:crypto';
import { AbstractRequest } from 'lib/request';
import { IHttpError, IRequest, REQUEST_HANDLER, ReqScope } from 'lib/types';
import { HttpError } from 'lib/http-error';

const header = {
  alg: 'HS256',
  typ: 'JWT',
};

const payload = {
  user: 'John Doe',
};

const secret = process.env.JWT_SECRET || '';

export class AuthRequest extends AbstractRequest {
  public process(
    req: IncomingMessage,
    scope: ReqScope
  ): IRequest | IHttpError | null {
    if (scope.currentStep === REQUEST_HANDLER.AUTH) {
      // Since by task we should generate JWT on front-end
      // AND we cannot decode HMAC to verify signature
      // the only way to verify JWT is to generate it with all known data and compare 🥲
      const token = req.headers['authorization']?.replace('Bearer ', '');
      const generatedToken = generateJWT(header, payload, secret);
      if (token !== generatedToken) {
        throw new HttpError(403, 'Unauthorized');
      }
    }

    return super.process(req, scope);
  }
}

const generateJWT = (header: object, payload: object, secret: string) => {
  const base64Header = Buffer.from(JSON.stringify(header))
    .toString('base64')
    .replaceAll('=', '');
  const base64Payload = Buffer.from(JSON.stringify(payload))
    .toString('base64')
    .replaceAll('=', '');
  const hmac = crypto.createHmac('sha256', secret);
  hmac.update(`${base64Header}.${base64Payload}`);
  const base64Sign = hmac.digest('base64').replaceAll('=', '');

  return `${base64Header}.${base64Payload}.${base64Sign}`;
};
