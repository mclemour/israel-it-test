'use strict';

import { IncomingMessage } from 'http';
import { AbstractRequest } from 'lib/request';
import { IHttpError, IRequest, REQUEST_HANDLER, ReqScope } from 'lib/types';
import { HttpError } from 'lib/http-error';

interface Schema {
  [key: string]: string;
}

const validate = (body: object): body is Schema => {
  return Object.values(body).every((el) => typeof el === 'string');
};

export class ValidateSchemaRequest extends AbstractRequest {
  public process(
    req: IncomingMessage,
    scope: ReqScope
  ): IRequest | IHttpError | null {
    if (scope.currentStep === REQUEST_HANDLER.VALIDATE_SCHEMA) {
      if (scope.body && !validate(scope.body)) {
        scope.err = new HttpError(400, 'Schema validation error');
        return scope.err;
      }
      if (scope.body && !scope.body.data) {
        scope.err = new HttpError(
          422,
          'Body should be in the next format: {"data": "some-string"}'
        );
        return scope.err;
      }
    }
    return super.process(req, scope);
  }
}
