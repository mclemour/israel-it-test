'use strict';

import { AuthRequest } from './auth';
import { ValidateSchemaRequest } from './validate-schema';

// Difine global chain of responsibility order handlers
const auth = new AuthRequest();
const validateSchema = new ValidateSchemaRequest();

auth.setNext(validateSchema);

export const routeBeginning = auth;
