'use strict';

import { IncomingMessage } from 'http';
import { IHttpError, IRequest, ReqScope } from './types';

export abstract class AbstractRequest implements IRequest {
  private _nextRequest!: IRequest;

  public setNext(request: IRequest): IRequest {
    this._nextRequest = request;
    // Returning from here allows chaining
    return request;
  }

  public process(
    req: IncomingMessage,
    scope: ReqScope
  ): IRequest | IHttpError | null {
    if (this._nextRequest) {
      return this._nextRequest.process(req, scope);
    }
    return null;
  }
}
