'use strict';

import { IHttpError } from './types';

export class HttpError extends Error implements IHttpError {
  code: number;

  constructor(code: number, message: string) {
    super(message);
    this.code = code;
  }
}
