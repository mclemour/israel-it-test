'use strict';

let keyCounter = 0;

export default class MultiMap<K extends object, V> extends Map {
  private _objectHashes;
  private _symbolHashes;
  private _publicKeys;

  constructor() {
    super();

    this._objectHashes = new WeakMap();
    this._symbolHashes = new Map();
    this._publicKeys = new Map();

    const [pairs] = arguments;
    if (pairs === null || pairs == undefined) return;

    if (typeof pairs[Symbol.iterator] !== 'function') {
      throw new TypeError(
        typeof pairs +
          ' is no iterable (cannot read property Symbol(Symbol.iterator))'
      );
    }

    for (const [keys, value] of pairs) {
      this.set(keys, value);
    }
  }

  private _getPublicKeys(keys: K, create: boolean = false) {
    if (!Array.isArray(keys)) {
      throw new TypeError('The keys parameter must be an array');
    }

    const privateKey = this._getPrivateKey(keys, create);
    let publicKey;
    if (privateKey && this._publicKeys.has(privateKey)) {
      publicKey = this._publicKeys.get(privateKey);
    } else if (create) {
      publicKey = [...keys];
      this._publicKeys.set(privateKey, publicKey);
    }

    return { privateKey, publicKey };
  }

  private _getPrivateKey(keys: Iterable<K>, create: boolean = false) {
    const privateKeys = [];
    for (const key of keys) {
      const hashes =
        typeof key === 'object' || typeof key === 'function'
          ? '_objectHashes'
          : typeof key === 'symbol'
          ? '_symbolHashes'
          : false;

      if (!hashes) {
        privateKeys.push(key);
      } else if (this[hashes].has(key)) {
        privateKeys.push(this[hashes].get(key));
      } else if (create) {
        const privateKey = `@@mm-ref-${keyCounter++}@@`;
        this[hashes].set(key, privateKey);
        privateKeys.push(privateKey);
      } else {
        return '';
      }
    }

    return JSON.stringify(privateKeys);
  }

  set(keys: K, value: V) {
    const { publicKey } = this._getPublicKeys(keys, true);
    return super.set(publicKey, value);
  }

  get(keys: K) {
    const { publicKey } = this._getPublicKeys(keys);
    return super.get(publicKey);
  }

  has(keys: K) {
    const { publicKey } = this._getPublicKeys(keys);
    return super.has(publicKey);
  }

  delete(keys: K) {
    const { publicKey, privateKey } = this._getPublicKeys(keys);
    return Boolean(
      publicKey &&
        super.delete(publicKey) &&
        this._publicKeys.delete(privateKey)
    );
  }

  clear() {
    super.clear();
    this._symbolHashes.clear();
    this._publicKeys.clear();
  }

  get size() {
    return super.size;
  }
}
