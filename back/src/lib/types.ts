'use strict';

import { IncomingMessage, ServerResponse } from 'node:http';
import MultiMap from './multi-map';

export enum METHOD {
  GET,
  POST,
  PUT,
  DELETE,
  OPTIONS,
}

export interface IHandler {
  process(req: IncomingMessage, res: ServerResponse): void;
}

export type HandlerCB = (
  req: IncomingMessage,
  res: ServerResponse,
  params?: object | null
) => unknown;

export type Router = {
  handlers: MultiMap<[string, METHOD], IHandler>;
  clear: () => void;
  register: (url: string, method: METHOD, cb: HandlerCB) => void;
  route: (req: IncomingMessage) => IHandler;
  missing: (req: IncomingMessage) => IHandler;
};

export interface IRequest {
  setNext(handler: IRequest): IRequest;
  process(req: IncomingMessage, scope: ReqScope): IRequest | IHttpError | null;
}

export enum REQUEST_HANDLER {
  VALIDATE_SCHEMA = 0,
  AUTH,
  PROCESS,
}

export type ReqScope = {
  routeBeginning?: IRequest;
  routeEnding?: IRequest;
  steps?: REQUEST_HANDLER[];
  currentStep?: REQUEST_HANDLER;
  err?: IHttpError;
  rawBody?: Uint8Array[];
  body?: {
    [key: string]: any;
  };
  params?: {
    offset?: string;
    limit?: string;
    order?: string;
  };
};

export type RouteCB = (req: IncomingMessage, scope: ReqScope) => unknown;

export interface IHttpError extends Error {
  code: number;
}

export enum KEY_VALUE_STORAGE {
  MOCK,
  REDIS_CLI_SHELL,
}

export interface IKeyValueStorage<K, V> {
  get: (key: K) => Promise<string>;
  set: (key: K, value: V) => Promise<string>;
  getset: (key: K, value: V) => Promise<V | undefined>;
  delete: (key: K) => Promise<string>;
  zadd: (collection: string, key: K) => Promise<string>;
  mget: (keys: K[]) => Promise<string[]>;
  zrange: (
    collection: string,
    offset: number,
    limit: number,
    order: 'asc' | 'desc'
  ) => Promise<string[]>;
  zcard: (collection: string) => Promise<number>;
}

export interface IDefaultQueryParams {
  offset: number;
  limit: number;
  order: 'asc' | 'desc';
}
