'use strict';

import { IncomingMessage, ServerResponse } from 'node:http';
import parser from 'node:url';
import {
  IHandler,
  HandlerCB,
  Router,
  METHOD,
  ReqScope,
  IRequest,
  RouteCB,
} from './types';
import { createHandler as handlerFactory } from './handler';
import MultiMap from './multi-map';
import { HttpError } from './http-error';

export const router: Router = {
  handlers: new MultiMap<[string, METHOD], IHandler>(),
  clear: function () {
    this.handlers.clear();
  },
  register: function (url: string, method: METHOD, cb: HandlerCB) {
    this.handlers.set([url, method], handlerFactory(cb));
  },
  route: function (req: IncomingMessage) {
    const url = parser.parse(req.url as string, true);
    const method = METHOD[req.method as keyof typeof METHOD];
    const handler = this.handlers.get([url.pathname || '', method]);

    if (!handler) return this.missing(req);

    return handler;
  },
  missing: (req: IncomingMessage) => {
    return handlerFactory((req: IncomingMessage, res: ServerResponse) => {
      res.writeHead(404, { 'Content-Type': 'text/plain' });
      res.write('Route not found');
      res.end();
    });
  },
};

export const route =
  (fn: RouteCB, scope: ReqScope) =>
  async (req: IncomingMessage, res: ServerResponse) => {
    const started = +new Date();
    const runWithScope = async (
      chain: IRequest | undefined,
      scope: ReqScope
    ) => {
      let lastResult = null;

      if (!scope.steps || !chain) return;

      for (const currentStep of scope.steps) {
        scope.currentStep = currentStep;
        lastResult = chain?.process(req, scope);

        if (lastResult instanceof HttpError || scope.err) {
          console.log(scope.err);
          throw scope.err;
        }
      }
    };

    req
      .on('data', (chunk) => {
        if (scope.rawBody) {
          scope.rawBody.push(chunk);
        } else {
          scope.rawBody = [chunk];
        }
      })
      .on('end', async () => {
        try {
          try {
            const bodyToProcess = scope.rawBody
              ? Buffer.concat(scope.rawBody).toString()
              : null;

            scope.body = bodyToProcess
              ? JSON.parse(decodeURI(bodyToProcess))
              : null;
          } catch (err) {
            scope.err = new HttpError(400, 'Cannot parse body');
            throw scope.err;
          }

          await runWithScope(scope.routeBeginning, scope);
          let result = await fn(req, scope);
          await runWithScope(scope.routeEnding, scope);

          if (!result) {
            result = { success: true };
          }

          res.writeHead(200, { 'Content-Type': 'application/json' });
          res.write(JSON.stringify(result));
          res.end();
        } catch (err) {
          console.log(err);
          const code = err instanceof HttpError ? err.code : 500;
          const msg = err instanceof HttpError ? err.message : err;
          res.writeHead(code, { 'Content-Type': 'application/json' });
          res.write(JSON.stringify(msg));
          res.end();
        }
      })
      .on('error', (err) => {
        const code = err instanceof HttpError ? err.code : 500;
        const msg = err instanceof HttpError ? err.message : err;
        res.writeHead(code, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify(msg));
        res.end();
      });

    const requestTime = +new Date() - started;
    console.log(`${req.method} ${req.url} ${requestTime}ms`);
  };
