'use strict';

import { IncomingMessage, ServerResponse } from 'node:http';
import { IHandler, HandlerCB } from './types';

export class Handler implements IHandler {
  process: (req: IncomingMessage, res: ServerResponse) => unknown;

  constructor(cb: HandlerCB) {
    this.process = function (req: IncomingMessage, res: ServerResponse) {
      const params = null;
      return cb.apply(this, [req, res, params]);
    };
  }
}

export const createHandler = (cb: HandlerCB): IHandler => {
  return new Handler(cb);
};
