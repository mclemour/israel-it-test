'use strict';

import { IncomingMessage } from 'http';
import { ReqScope } from 'lib/types';
import { keyValueStorageFactory } from 'services/key-value-storage/_index';
import { KEY_VALUE_STORAGE } from 'lib/types';
import { DEFAULT_QUERY_PARAMS } from 'services/key-value-storage/redis-cli-shell';

const COLLECTION = 'redis-test-collection';

export const redisList = async (req: IncomingMessage, scope: ReqScope) => {
  const offset = Number(scope.params?.offset) || DEFAULT_QUERY_PARAMS.offset;
  const limit = Number(scope.params?.limit) || DEFAULT_QUERY_PARAMS.limit;
  const order =
    (scope.params?.order as 'asc' | 'desc') || DEFAULT_QUERY_PARAMS.order;

  const storage = new (keyValueStorageFactory<string, string>(
    KEY_VALUE_STORAGE.REDIS_CLI_SHELL
  ))();
  const keys = await storage.zrange(COLLECTION, offset, limit, order);
  const total = await storage.zcard(COLLECTION);
  const values = [];

  // This cycle is unnecessary because of existance MGET
  // BUT RESP protocol should be parsed properly
  // it's much easier to retrieve recods one by one
  for (const key of keys) {
    values.push(await storage.get(key));
  }

  return {
    data: values,
    total,
  };
};
