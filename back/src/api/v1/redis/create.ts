'use strict';

import { IncomingMessage } from 'http';
import crypto from 'node:crypto';
import { KEY_VALUE_STORAGE, ReqScope } from 'lib/types';
import { keyValueStorageFactory } from 'services/key-value-storage/_index';

const COLLECTION = 'redis-test-collection';

export const redisCreate = async (req: IncomingMessage, scope: ReqScope) => {
  if (!scope.body || !scope.body.data) return;

  const storage = new (keyValueStorageFactory<string, string>(
    KEY_VALUE_STORAGE.REDIS_CLI_SHELL
  ))();
  const data = scope.body.data;
  const key = crypto.randomBytes(20).toString('hex');
  await storage.set(key, data);
  await storage.zadd(COLLECTION, key);
};
