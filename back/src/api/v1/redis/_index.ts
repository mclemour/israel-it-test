'use strict';

import parser from 'node:url';
import { router, route } from 'lib/router';
import { METHOD, REQUEST_HANDLER } from 'lib/types';
import { routeBeginning } from 'request-handlers/_index';
import { redisCreate } from './create';
import { redisList } from './list';

router.register('/api/v1/redis', METHOD.POST, async (req, res) => {
  // Define steps being using from chain of responsibility for current request
  const steps = [REQUEST_HANDLER.VALIDATE_SCHEMA];
  const scope = {
    routeBeginning,
    steps,
  };
  return route(redisCreate, scope)(req, res);
});
router.register('/api/v1/redis', METHOD.GET, async (req, res) => {
  const steps = [REQUEST_HANDLER.AUTH];
  const url = parser.parse(req.url as string, true);
  const scope = {
    routeBeginning,
    steps,
    params: url.query,
  };
  return route(redisList, scope)(req, res);
});

// Since client sends not-so-simple request, it needs to handle preflight HTTP OPTIONS request
router.register('/api/v1/redis', METHOD.OPTIONS, async (req, res) => {
  return route(() => {}, {})(req, res);
});
