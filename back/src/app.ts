'use strict';

import http from 'node:http';
import { router } from './lib/router';

const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 8000;

require('./api/v1/_index');

const server = http.createServer((req, res) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Request-Method', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, POST, GET');
  res.setHeader('Access-Control-Allow-Headers', '*');

  const handler = router.route(req);
  handler.process(req, res);
});

server.listen(port);

process.on('SIGINT', () => {
  // shout down all active connections (if any)
});
