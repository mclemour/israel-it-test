## Getting started

Clone repository:

```bash
> git clone https://gitlab.com/mclemour/israel-it-test.git
```

Run application:

```bash
# goto cloned repository
> cd ./israel-it-test
# grant permissions
> sudo chmod +x ./compose.sh
# start services
> ./compose.sh up -d
```
Fill database via `process.ts`:
```bash
> npx ts-node process.ts
```

Links: 
- Front-end [localhost:5173](localhost:5173)
- Back-end [localhost:8000](localhost:8000)